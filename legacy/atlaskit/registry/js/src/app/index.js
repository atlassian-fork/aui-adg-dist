import { Component, define, prop, h } from 'skatejs'; // eslint-disable-line

// AtlasKit components
import AkPage from 'ak-page';
import { default as AkNavigation, NavigationLink } from 'ak-navigation';
import AtlassianIcon from 'ak-icon/glyph/atlassian';
import DashboardIcon from 'ak-icon/glyph/dashboard';
import ReposIcon from 'ak-icon/glyph/bitbucket/repos';
import BranchesIcon from 'ak-icon/glyph/bitbucket/branches';
import BitbucketIcon from 'ak-icon/glyph/bitbucket/logo';
import SnippetsIcon from 'ak-icon/glyph/bitbucket/snippets';
import FeedbackIcon from 'ak-icon/glyph/feedback';
import {
  akFontFamily as fontFamily,
  akFontSizeDefault as fontSize,
} from 'akutil-shared-styles';

import Module from '../module';
import Route from '../route';
import { BASE_URL } from '../config';
import Footer from '../components/footer';
import handleClick from '../util/handleClick';

export default define('ak-registry-app', class extends Component {
  static get props() {
    return {
      rerenderHack: prop.number(),
      page: {},
    };
  }
  static attached(elem) {
    elem.addEventListener('click', handleClick(elem));
    window.addEventListener('popstate', () => {
      elem.rerenderHack += 1;
    });
  }
  static render(elem) {
    const { rerenderHack } = elem;
    const Page = elem.page;
    const render = (pg) => {
      elem.page = pg.default;
    };

    const route = (path, bundle) => (
      <Route
        path={`${BASE_URL}${path}`}
        rerenderHack={rerenderHack}
        match={() =>
          <Module
            load={require(`bundle!../pages/${bundle}`)} // eslint-disable-line
            done={render}
          />
        }
      />
    );

    return (
      <AkPage>
        <style>{`
        * {
          font-family: ${fontFamily};
          font-size: ${fontSize}
        }
        `}</style>
        <AkNavigation
          open
          slot="navigation"
          container-logo={`${BASE_URL}/img/nucleus.png`}
          container-name="AtlasKit Registry"
          container-href={`${BASE_URL}/`}
        >
          <AtlassianIcon slot="global-home" style={{ zoom: 1.25 }} />

          <NavigationLink href={`${BASE_URL}/`}>
            <DashboardIcon slot="icon" />
            Components
          </NavigationLink>

          <NavigationLink href={`${BASE_URL}/morty.html`}>
            <SnippetsIcon slot="icon" />
            Bundler
          </NavigationLink>

          <NavigationLink href={`${BASE_URL}/metrics.html`}>
            <ReposIcon slot="icon" />
            Metrics
          </NavigationLink>

          <NavigationLink href={`${BASE_URL}/dependencies.html`}>
            <BranchesIcon slot="icon" />
            Dependencies
          </NavigationLink>

          <NavigationLink href="https://bitbucket.org/atlassian/atlaskit">
            <BitbucketIcon slot="icon" />
            Bitbucket Repo
          </NavigationLink>

          <NavigationLink href="https://ecosystem.atlassian.net/projects/AK/">
            <FeedbackIcon slot="icon" />
            AtlasKit JIRA
          </NavigationLink>
        </AkNavigation>

        <div>{Page ? <Page rerenderHack={rerenderHack} /> : ''}</div>

        <Footer />

        {route('/', 'component_index')}
        {route('/index.html', 'component_index')}
        {route('/:name/:version/index.html', 'component_detail')}
        {route('/metrics.html', 'metrics')}
        {route('/dependencies.html', 'dependencies')}
        {route('/morty.html', 'morty')}
      </AkPage>
    );
  }
});
