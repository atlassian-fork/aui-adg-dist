import { define } from 'skatejs';

export default define('akr-module', {
  props: {
    args: {},
    load: {},
    done: {},
  },
  updated(elem) {
    if (typeof elem.load === 'function') {
      elem.load(args => (elem.args = args));
    }
    return elem.args;
  },
  render(elem) {
    if (typeof elem.done === 'function') {
      return elem.done(elem.args);
    }
    return null;
  },
});
