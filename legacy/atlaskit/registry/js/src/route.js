import { define, prop } from 'skatejs';
// import page from 'page';

const registered = [];

function routePathMatchesCurrentUrl(path) {
  const pathAsRegex = new RegExp(`^${path.replace(/:[a-zA-Z]+/g, '.+')}$`);
  return pathAsRegex.test(window.location.pathname);
}

export default define('akr-router-route', {
  props: {
    rerenderHack: prop.number({
      set: (elem) => {
        elem.matched = routePathMatchesCurrentUrl(elem.path);
      },
    }),
    match: {},
    matched: prop.boolean({ attribute: true }),
    path: prop.string({ attribute: true }),
  },
  updated(elem) {
    const { path } = elem;

    if (registered.indexOf(path) === -1) {
      registered.push(path);
      window.onpopstate = () => {
        elem.matched = routePathMatchesCurrentUrl(path);
      };

      elem.matched = routePathMatchesCurrentUrl(path);
    }

    return true;
  },
  render(elem) {
    return elem.matched && elem.match();
  },
});
