/* eslint-disable react/no-unknown-property */

import {
  define,
  prop,
  h, // eslint-disable-line no-unused-vars
} from 'skatejs';

const sharedStyles = `
  .atlaskit-registry-sparkline {
    color: red;
  }

  .spark-fill {
    stroke: none;
    stroke-width: 0;
    fill-opacity: .15;
    fill: #1c8cdc;
  }

  .spark-outline {
    stroke: #1c8cdc;
    stroke-width: 1;
    stroke-linejoin: round;
    stroke-linecap: round;
    fill: none;
  }

  .spark-bottom-line {
    fill: none;
    stroke: #666;
    stroke-width: 1;
  }
`;

function generatePointsFromData(width, height, data) {
  let maxValue = 0;
  data.forEach((d) => {
    if (d > maxValue) maxValue = d;
  });

  const xIncrement = data.length > 1 ? width / (data.length - 1) : 0;

  const points = data.map((d, i) => {
    const x = i * xIncrement;
    const dWeight = maxValue ? d / maxValue : 0;
    const y = height - (height * dWeight);
    return `${x} ${y}`;
  });

  return points.join(' ');
}

export default define('atlaskit-registry-sparkline', {
  props: {
    values: prop.string({
      attribute: true,
      default: '0,1,2,3,4,5',
    }),
    reverse: prop.boolean({
      attribute: true,
    }),
  },
  render(elem) {
    const width = 90;
    const height = 20;

    const splitData = elem.values.split(',').map(x => parseInt(x, 10));
    if (elem.reverse) splitData.reverse();
    const dataPoints = generatePointsFromData(width, height, splitData);

    return (
      <div class="atlaskit-registry-sparkline">
        <style>{sharedStyles}</style>
        <svg height={height} width={width}>
          <polyline
            points={`${dataPoints} ${width} ${height} 0 ${height}`}
            class="spark-fill"
          />
          <polyline
            points={dataPoints}
            class="spark-outline"
          />
          <polyline
            points={generatePointsFromData(width, height, [0, 0, 0])}
            class="spark-bottom-line"
          />
          Sorry, your browser does not support inline SVG.
        </svg>
      </div>
    );
  },
});
