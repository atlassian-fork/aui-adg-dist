import { define, prop, props, h } from 'skatejs'; // eslint-disable-line
import { default as Tabs, Tab } from 'ak-tabs';
import { TABS } from '../config';
import Spinner from './spinner';

const tableStyle = require('!css!less!../css/table.less'); // eslint-disable-line
const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line

function filterTabContent(tab, search) {
  if (!search) {
    return tab.filter;
  }

  return component => component.name.match(search);
}

export default define('akr-tabbable-content', {
  props: {
    components: prop.array(),
    search: prop.string(),
    header: {},
    rowFn: {},
  },
  render(elem) {
    const { components, rowFn, search, header: Header } = props(elem);
    if (!rowFn || !components || !components.length) return <Spinner />;

    return (
      <div>
        <style>
          {tableStyle.toString()}
          {typeStyle.toString()}
        </style>
        <Tabs>
          {
            TABS.map(tab => (
              <Tab label={tab.title} selected={tab.selected}>
                { <Header /> || null }
                {
                  components.filter(filterTabContent(tab, search))
                    .sort(tab.sort)
                    .map(tabComponent => rowFn(tabComponent))
                }
              </Tab>
            ))
          }
        </Tabs>
      </div>
    );
  },
});
