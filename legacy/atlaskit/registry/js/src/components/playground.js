import { define, prop, h } from 'skatejs'; // eslint-disable-line no-unused-vars
import Sandy from 'sandy';
import Button from 'ak-button';

const MORTY_BASE_URL = 'https://eovrzrbij2.execute-api.ap-southeast-2.amazonaws.com/dev/bundle.js';

// Fetch the JSON file containing the example code
function fetchRemoteSources(pkg, version) {
  return fetch(`https://api.bitbucket.org/1.0/repositories/atlassian/atlaskit/raw/${pkg}@${version}/packages/${pkg}/src/index.js`)
    .then(res => res.text())
    .catch(() => null);
}

function createSandy(config) {
  return Sandy(config); // eslint-disable-line new-cap
}

function openSandy(sandy, destination) {
  sandy.pushTo(destination);
}

function getPlaygroundRegex(type) {
  return new RegExp(`@example @${type} @playground([\\s\\S]*?)(@|\\*\\/)`);
}

function getPlaygroundData(raw) {
  const data = {};
  ['html', 'js', 'css'].forEach((type) => {
    const re = getPlaygroundRegex(type);
    const result = re.exec(raw);
    if (result) {
      data[type] = result[1].replace(/ \*/g, '');
    }
  });
  return data;
}

function dataToConfig(data) {
  const config = {};
  Object.keys(data).forEach((key) => {
    config[key] = { content: data[key] };
  });
  return config;
}

function setupSandy(elem) {
  if (!elem.pkg || !elem.version) { return; }
  fetchRemoteSources(elem.pkg, elem.version).then((raw) => {
    if (raw) {
      const data = getPlaygroundData(raw);
      if (Object.keys(data).length > 0) {
        const config = dataToConfig(data);
        config.dependencies = [
          `${MORTY_BASE_URL}?packages=skatejs-web-components,${elem.pkg}@${elem.version}`,
        ];
        elem.sandy = createSandy(config);
      }
    }
  });
}

export default define('atlaskit-registry-playground', {
  attached: setupSandy,
  render(elem) {
    return (elem.sandy ?
      <Button
        appearance="primary"
        onClick={() => openSandy(elem.sandy, 'codepen')}
      >Edit in CodePen</Button> : null
    );
  },
  props: {
    pkg: prop.string({
      attribute: true,
      set: setupSandy,
    }),
    version: prop.string({
      attribute: true,
      set: setupSandy,
    }),
    sandy: {},
  },
});
