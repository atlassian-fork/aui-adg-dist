import { define, prop, props, h } from 'skatejs'; // eslint-disable-line
import Lozenge from 'ak-lozenge';
import { default as Dropdown, DropdownTriggerButton, Item as DropdownItem } from 'ak-dropdown';
import RelativeDate from '../components/relative-date';
import Playground from '../components/playground';
import Spinner from '../components/spinner';
import { BASE_URL, STORY_BASE_URL } from '../config';

const tableStyle = require('!css!less!../css/table.less'); // eslint-disable-line
const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line
const componentDetailStyle = require('!css!less!../css/component-detail.less'); // eslint-disable-line

export default define('akr-page-component-detail', {
  props: {
    rerenderHack: prop.number({
      set(elem) {
        const regex = new RegExp(`^${BASE_URL}/([^/]+)/([^/]+)`);

        // eslint-disable-next-line no-unused-vars
        const match = window.location.pathname.match(regex);
        if (match) {
          const [fullMatch, componentName, componentVersion] = match; // eslint-disable-line
          const componentTag = `${componentName}@${componentVersion}`;

          if (componentTag !== elem.componentTag) {
            props(elem, { componentName, componentVersion, componentTag });
            fetch(`${BASE_URL}/spa_data/${componentTag}.json`)
              .then(result => result.json())
              .then((json) => {
                elem.data = json;
              });
          }
        }
      },
    }),
    data: {},
    componentName: prop.string(),
    componentVersion: prop.string({
      set(elem, data) {
        if (data.oldValue !== data.newValue) {
          props(elem, {
            data: false,
          });
        }
      },
    }),
    componentTag: prop.string(),
  },
  attached(elem) {
    elem.rerenderHack += 1; // needed for polyfilled browsers e.g. Firefox
  },
  render(elem) {
    const { componentName, componentTag, data } = elem;
    if (!data) {
      return (
        <div>
          <h2>{componentName}</h2>
          <Spinner />
        </div>
      );
    }

    const { cssStats: css, jsStats: js } = data;

    return (
      <div>
        <style>
          {tableStyle.toString()}
          {typeStyle.toString()}
          {componentDetailStyle.toString()}
        </style>
        <h2>
          {componentName}
          &nbsp;
          <Dropdown style={{ display: 'inline-block', 'font-size': '14px' }}>
            <DropdownTriggerButton slot="trigger">{data.version}</DropdownTriggerButton>
            <DropdownItem
              href={`${BASE_URL}/${componentName}/latest/index.html`}
              checked
            >
              Latest
            </DropdownItem>
            {
              data.allVersions.map(version => (
                <DropdownItem
                  href={`${BASE_URL}/${componentName}/${version}/index.html`}
                >
                  {version}
                </DropdownItem>
              ))
            }
          </Dropdown>
        </h2>
        <h3>Component info</h3>
        <Playground
          style={{ float: 'right' }}
          pkg={componentName}
          version={data.version}
        />
        <div class="row">
          <div class="col-5">
            <dl>
              <dt>NPM install</dt>
              <dd>
                <code>npm install {componentTag} --save</code>
              </dd>
              <dt>Documentation</dt>
              <dd>
                <a href={`https://www.npmjs.com/package/${componentName}`}>Docs on NPM</a>
              </dd>
              <dt>This version</dt>
              <dd>
                <a href={`https://npmjs.com/package/${componentName}`}>{data.version}</a> (<RelativeDate iso={data.npmInfo.publishTime} />)
                <Lozenge appearance="success">Published</Lozenge>
              </dd>
              <dt>Source code</dt>
              <dd>
                <a href={`https://bitbucket.org/atlassian/atlaskit/src/master/packages/${componentName}`}>Bitbucket</a>
              </dd>
              <dt>Bundle</dt>
              <dd>
                <a href={`https://unpkg.com/${componentName}@${data.version}/dist/`}>unpkg dist</a>
              </dd>
            </dl>
          </div>


          <div class="col-5">
            <dl>
              <dt>CSS size</dt>
              <dd>
                {css.size} bytes ({css.gzipSize} bytes gzipped)
              </dd>
              <dt>CSS counts</dt>
              <dd>
                {css.ruleCount} rules, {css.selectorCount} selectors,
                &nbsp;
                {css.declarationCount} declarations
              </dd>
              <dt>CSS specificity</dt>
              <dd>
                {css.averageSelectorSpecificity} avg, {css.maxSelectorSpecificity} max
              </dd>
              <dt>JS size</dt>
              <dd>
                {js.size} characters
              </dd>
            </dl>

          </div>
        </div>

        <h3 style={{ 'padding-top': '80px' }}>
          Storybook (<a href={`${STORY_BASE_URL}/${componentName}/${data.version}/`} target="_blank" rel="noreferrer noopener">this version</a>)
        </h3>
        <p>
          <iframe
            style={{ width: '100%', 'min-height': '600px', border: '1px solid #DDD' }}
            src={`${STORY_BASE_URL}/${componentName}/${data.version}/`}
          />
        </p>

        <h3 id="release-notes" style={{ 'padding-top': '80px' }}>
          Release notes (<a href={`https://unpkg.com/${componentName}@${data.version}/CHANGELOG.md`} target="_blank" rel="noreferrer noopener">raw markdown</a>)
        </h3>
        <p>
          <iframe
            style={{ width: '100%', 'min-height': '600px', border: '1px solid #DDD' }}
            src={`https://markdown-renderer-27442.firebaseapp.com/?url=https://unpkg.com/${componentTag}/CHANGELOG.md&stylesheet=TODO/css/main.css?t=TODO`}
          />
        </p>
      </div>
    );
  },
});
