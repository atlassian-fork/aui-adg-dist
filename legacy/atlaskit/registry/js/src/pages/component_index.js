import { define, prop, props, h } from 'skatejs'; // eslint-disable-line
import Lozenge from 'ak-lozenge';
import { BASE_URL } from '../config';
import RelativeDate from '../components/relative-date';
import TabContent from '../components/tab_content';

const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line
const componentIndexStyle = require('!css!less!../css/component-index.less'); // eslint-disable-line
const inputField = Symbol('inputField');
const input = Symbol('input');

function filterChange(elem) {
  return () => {
    elem[inputField] = elem[input].value;
  };
}

export default define('akr-page-components', {
  props: {
    components: prop.array(),
    [inputField]: prop.string(),
  },
  attached(elem) {
    fetch('/atlaskit/registry/spa_data/components_page.json')
      .then(result => result.json())
      .then((json) => {
        elem.components = json;
      });
  },
  render(elem) {
    const { components } = elem;

    function row(component) {
      const PlatformLozenge = component.platform ? (
        <Lozenge
          appearance={component.platform === 'React' ? 'new' : 'moved'}
        >{component.platform}</Lozenge>
      ) : <span>&nbsp;</span>;

      return (
        <div class="row table-row">
          <div class="col-5">
            <a href={`${BASE_URL}/${component.name}/latest/index.html`}>{component.name}</a><br />
          </div>
          <div class="col-1">
            <PlatformLozenge />
          </div>
          <div class="col-2">
            <a href={`https://www.npmjs.com/package/${component.name}`}>
              {component.version}
            </a>
            &nbsp;
            (<RelativeDate iso={component.publishTime} />)
          </div>
          <div class="col-2">
            <Lozenge appearance="success">Published</Lozenge>
          </div>
        </div>
      );
    }

    return (
      <div>
        <style>
          {typeStyle.toString()}
          {componentIndexStyle.toString()}
        </style>
        <input
          type="text"
          placeholder="Filter components..."
          onKeyup={filterChange(elem)}
          ref={el => (elem[input] = el)}
          class="filter-input"
        />
        <h2>Components</h2>
        <TabContent
          components={components}
          search={elem[inputField]}
          rowFn={row}
        />
      </div>
    );
  },
});
