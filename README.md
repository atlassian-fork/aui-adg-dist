# aui-adg-dist

An extension of [AUI](https://bitbucket.org/atlassian/aui/) to implement the Atlassian Design Guidelines ([ADG](https://design.atlassian.com/)).

This repository only contains auto-generated release bundles of the [AUI](https://docs.atlassian.com/aui/) library with ADG extensions. See https://bitbucket.org/atlassian/aui-adg/ for the full source code.

# Builds
* [Plan Template build](https://deployment-bamboo.internal.atlassian.com/browse/FAB-AADT)
* [Deployment build](https://deployment-bamboo.internal.atlassian.com/deploy/viewDeploymentProjectEnvironments.action?id=327352352)

# Legacy Assets
Some of the contents of this new CDN were simply migrated from the original. Getting the original source or builds working against this new Micros managed CDN was not worth the effort and so these assets are simply stored in this repository and uploaded from here via a deployment build.

TODO - reference the build

## License

AUI ADG is released under the [Atlassian Design Guidelines License Agreement](https://bitbucket.org/atlassian/aui-adg/src/master/LICENSE).
