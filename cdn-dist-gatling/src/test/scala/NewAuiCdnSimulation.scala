class NewAuiCdnSimulation extends AuiResourcesBase {
  override def getProtocol: String = "https"

//  override def getAddress: String = "aui-cdn.prod.atl-paas.net"

  override def getAddress: String = "aui-cdn-test.atlassian.com"

  override def getResource: String = "js/aui.min.js"
}
