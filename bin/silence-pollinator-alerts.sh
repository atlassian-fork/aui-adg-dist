#!/usr/bin/env bash

# A script to change the notification settings on all the Pollinator checks so that no notifications are fired
# The original configuration of the checks will be saved to monitoring/pollinator-checks.yml for restoring the configuration
# afterwards

set -e

echo 'Ensure pollidev login has been run first.'

uuids=($(pollidev search --no-headers "Fabric AUI" | awk '{print $1}'))

for uuid in ${uuids[*]};
    do
        pollidev import -f ../monitoring/pollinator-checks.yml $uuid
        echo 'Imported the check for uuid ' $uuid
    done

echo 'The original checks have been written to ../monitoring/pollinator-checks.yml'
echo 'The checks with notifications disabled will be writen to ../monitoring/pollinator-checks-no-notification.yml'

remove_notifications_awk_prog='
$1 == "notifications:"{t=1;print $0 " []"}
t==1 && /notify_interval.*$/ {t=0;print $0;next}
t==1 {next}
t==0 {print $0}
'

awk "$remove_notifications_awk_prog" ../monitoring/pollinator-checks.yml > ../monitoring/pollinator-checks-no-notification.yml

echo 'Pushing checks with notifications removed.'
pollidev push --all -f ../monitoring/pollinator-checks-no-notification.yml

echo 'Remember to restore the notifications with the command pollidev push --all -f ../monitoring/pollinator-checks.yml'