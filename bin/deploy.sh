#!/usr/bin/env bash

# print commands (-x) and stop execution on error (-e)
set -xe

if [ -z "$1" ]; then
    echo "Usage: $0 [dev-west2|stg-west2|prod-west2]"
    exit 1
fi

# Make micros cli available
npm i -g @atlassian/micros-cli

micros static:deploy aui-cdn -e $1 -f aui-cdn.sd.yaml && micros static:invalidate aui-cdn -e $1 -f aui-cdn.sd.yaml