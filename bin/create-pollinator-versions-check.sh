#!/bin/bash

# A script to create a pollinator synthetic check that ensures all expected versions of AUI ADG are available.
# You need to have installed the Pollinator CLI (https://extranet.atlassian.com/display/OBSERVABILITY/How+To%3A+Pollinator+CLI#installation--421128575)
# and have performed pollidev login first
#

# print commands (-x) and stop execution on error (-e)
set -xe

cat ../monitoring/pollinator-versions-synthetic-check-template.yml | pollidev create --interactive=false --no-save -
